#! /usr/bin/env python
##Python 2.x program to convert text to sentiment
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
##Copyright (c) 2018, MUHAMMED RULSHID.S, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License

import rospy

import actionlib

import sentiment_analyzer_mode3.msg



class SpeechAction(object):
    # create messages that are used to publish feedback/result
    _feedback = sentiment_analyzer_mode3.msg.speechFeedback()
    _result = sentiment_analyzer_mode3.msg.speechResult()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, sentiment_analyzer_mode3.msg.speechAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
      
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # publish info to the console for the user
        rospy.loginfo('performing sentimental analysis of text %s',goal.text)
        x=goal.text
       
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
           
            # publish the feedback
        positive_word_list = ["good morning","hello","how are you"]

        neutral_word_list = ["but","not","america"]

        negative_word_list = ["you fool","idiot","killer"] 
        for i in range(0,3):
           if x== neutral_word_list[i]:
           	self._feedback.sequence='It is a nuetral word'
           	self._as.publish_feedback(self._feedback)
           if x== positive_word_list[i]:
           	self._feedback.sequence='It is a positive word'
           	self._as.publish_feedback(self._feedback)
           if x== negative_word_list[i]:
           	self._feedback.sequence='It is a negative word'
           	self._as.publish_feedback(self._feedback)
            # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
        r.sleep()
          
        if success:
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
    rospy.init_node('speech')
    server = SpeechAction(rospy.get_name())
    rospy.spin()

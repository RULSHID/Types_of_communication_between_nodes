#!/usr/bin/env python
#!/usr/bin/env python
##Python 2.x program
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
##Copyright (c) 2018, MUHAMMED RULSHID.S, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License

from sentiment_analyzer_mode2.srv import *

import rospy

#handle function definition
def handle_analysis(req):
#creating neutral, positive and negative word list
    neutral=['america','but','day','time']
    positive=['good morning','happy','beautiful','good']
    negative=['hell','bad','cry','you fool']
    #getting data from client
    z=req.a
    print "converted text is ",req.a
    print "Returning"
    #performing sentimental analysis of converted text
    for i in range(0,4):
      if z==neutral[i]:
        output='The text is neutral'
      if z==positive[i]:
        output='The text is positive'
      if z==negative[i]:
        output='The text is negative'
#returning the result back to client
    return SpeechResponse(output)

def speech_server():
#creating server node
    rospy.init_node('speech_server')
    s = rospy.Service('analysis', Speech, handle_analysis)
    print "Analysis is ready"
    rospy.spin()
#start of main function
if __name__ == "__main__":
    speech_server()

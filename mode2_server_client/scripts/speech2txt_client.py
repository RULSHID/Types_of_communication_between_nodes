#!/usr/bin/env python
##Python 2.x program
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
##Copyright (c) 2018, MUHAMMED RULSHID.S, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License
from __future__ import print_function

import sys
import rospy
from sentiment_analyzer_mode2.srv import *
import speech_recognition as sr


def speech_client(y):
#convenience method that blocks until the service named analysis is available.
    rospy.wait_for_service('analysis')
    try:
        #create a handle for calling the service: 
        analysis = rospy.ServiceProxy('analysis', Speech)
        #calling handle
        resp1 = analysis(y)
        return resp1.analysis
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

# main function starts
if __name__ == "__main__":
  while 1:   
    r = sr.Recognizer()
#make previously recorded audio file as source
    with sr.Microphone() as source:
         audio = r.listen(source)
#Audio is converted into text
    y=r.recognize_google(audio)
#print the analyzed result from server
    print speech_client(y)

#! /usr/bin/env python
##Python 2.x program to convert speech to text
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
##Copyright (c) 2018, MUHAMMED RULSHID.S, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License
from __future__ import print_function

import rospy
# Brings in the SimpleActionClient
import actionlib

import speech_recognition as sr

# Brings in the messages used by the speech action, including the
# goal message and the result message.
import sentiment_analyzer_mode3.msg

def speech2txt_client():
    # Creates the SimpleActionClient, passing the type of the action
    # (SpeechAction) to the constructor.
    client = actionlib.SimpleActionClient('speech', sentiment_analyzer_mode3.msg.speechAction)
    
    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()
    
    r = sr.Recognizer()
    with sr.Microphone() as source:
         audio = r.listen(source)
    y=r.recognize_google(audio)
    # Creates a goal to send to the action server.
    goal = sentiment_analyzer_mode3.msg.speechGoal(text=y)
    #feedback=speech2txt_action.msg.SpeechFeedback(sequence=y)
    #client.send_feedback(feedback)
    # Sends the goal to the action server.
    client.send_goal(goal)
    

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result() 

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('speech_client')
        result = speech2txt_client()
	while not rospy.is_shutdown():
		print("Result:",result.sequence)
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)


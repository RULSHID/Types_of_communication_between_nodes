 COMMUNICATION  TYPES IN ROS


Base unit in ROS is called a node . These nodes communicate with each other by using topics or services, So here i 'm using a simple "sentiment_analyzer " program to demonstrate the three possible types of communication between nodes

Nodes:

    Base unit in ROS is called a node. Nodes are in charge of handling devices or computing algorithms- each node for separate task. Nodes can communicate with each other using topics or services. ROS software is distributed in packages. Single package is usually developed for performing one type of task and can contain one or multiple nodes.


Topics:

    In ROS, topic is a data stream used to exchange information between nodes. They are used to send frequent messages of one type. This could be a sensor readout or motor goal speed. Each topic is registered under the unique name and with defined message type. Nodes can connect with it to publish messages or subscribe to them. For a given topic, one node can not publish and subscribe to it at the same time, but there is no restrictions in the number of different nodes publishing or subscribing.

Services:

    Communication by services resemble client-server model. In this mode one node (the server) registers service in the system. Later any other node can ask that service and get response. In contrast to topics, services allow for two-way communication, as a request can also contain some data.


 Communication between nodes:
 
1. Using Publisher and Subscriber 
2. Using Service and Client
3. Using Action Client


##There is somthing wrong with these folders 
 "mode1_publisher_subscriber @ 403ab50c" 
 "mode3_action_client @ ed6959aa"
 but it doesnot require for the operation of the system , so you dont need to bother about it if it is not able to open!!

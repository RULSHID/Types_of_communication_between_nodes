elcome to the "Sentiment  analyzer" project using "server client"


It is  a  python2.7 based program in ROS  environment using action client server  in which the system takes speech  as input (by using microphone) and convert it into text  and display the sentiment in the text (whether it belongs to positive ,negative or neutral words) by comparing it with predefined set of  words . 


For example, If the user says “good morning”  then the output of the system will be : “it is a positive word”

It is similar to the prvious project "sentiment_analyzer_in_ros" but in this project we are using action client server for communication between the nodes, Advantage of this method is it provides feedback system
 


You need to  install ros in your Linux os ( i’m using ubuntu 16.04) by using this link: http://wiki.ros.org/kinetic/Installation/Ubuntu

Requirements:

1.Pip
Run the following command to update the package list and upgrade all of your system software to the latest version available

 sudo apt-get update && sudo apt-get -y upgrade

then install pip
 
 sudo apt-get install python-pip

2.PyAudio :

 sudo apt-get install portaudio19-dev python-all-dev python3-all-dev && sudo pip install pyaudio            
                                                                                                                                 
3.SpeechRecognition 3.8.1:

 pip install SpeechRecognition

 Follow this link for more deatils:https://pypi.org/project/SpeechRecognition/


After installing all the above things
 
  Follow these steps:

1. Create a catkin workspace

 Follow the link: http://wiki.ros.org/catkin/Tutorials/create_a_workspace

2.Create a catkin Package

 Follow the link: http://wiki.ros.org/ROS/Tutorials/CreatingPackage
3.Create a Service and Client

   Follow the link : http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28python%29

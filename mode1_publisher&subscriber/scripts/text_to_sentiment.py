#!/usr/bin/env python
##Python 2.x program to convert text to sentiment
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
##Copyright (c) 2018, MUHAMMED RULSHID.S, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License.

import rospy

from std_msgs.msg import String
#Defining list of possible words
positive_word_list = [
  "good morning",
  "hello",
  "how are you"
  
]

neutral_word_list = [
    "but",
    "not",
    "america"
]

negative_word_list = [
    "you fool",
    "idiot",
    "killer"
] 
 
def callback_and_sentiment_detection(data):   
     speech_str=data.data
     for i in range(0,3):
        #Check for the presence of received data in positive list
    	if speech_str == positive_word_list[i]:

			output_str='it is positive word'
                        #Print 'it is positive'
			rospy.loginfo(output_str)

    	if speech_str == neutral_word_list[i]:

			output_str='it is neutral word'

			rospy.loginfo(output_str)

    	if speech_str == negative_word_list[i]:

			output_str='it is negative word'

			rospy.loginfo(output_str)

def listener():
    
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'text_to_sentiment' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('text_to_sentiment', anonymous=True)
    #Creating Subscriber node with topic name'speech_str'
    rospy.Subscriber('speech_str', String, callback_and_sentiment_detection)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
#Start of main function
if __name__ == '__main__':
    listener()

#!/usr/bin/env python
##Python 2.x program to convert speech to text
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
##Copyright (c) 2018, MUHAMMED RULSHID.S, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License.

import rospy

from std_msgs.msg import String

import speech_recognition as sr

def talker_and_speech_rcognition():
    #Creating publisher node with topic name'speech_str'
    pub = rospy.Publisher('speech_str', String, queue_size=10)
    ##Create node with name speechnode. 
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'speech_to_text' node so that multiple listeners can
    # run simultaneously
    rospy.init_node('speech_to_text', anonymous=True)
    rate = rospy.Rate(10)
    ##Speech recognition function
    r = sr.Recognizer()
    #Using microphone as the source of voice
    with sr.Microphone() as source:
	#Reads the audio file
    	audio = r.listen(source)
        #Converting the speech into text by using google api
    	speech_str=r.recognize_google(audio)
    #Execute program until there is an interrupt ctrl-c   
    while not rospy.is_shutdown():
        #Messages get printed to screen
        rospy.loginfo(speech_str)
        #Publishes a string to our 'speech_str' topic
        pub.publish(speech_str)
        rate.sleep()
# Start main function
if __name__ == '__main__':
    # calling talker_and_speech_rcognition function
    try:
        talker_and_speech_rcognition()
    except rospy.ROSInterruptException:
        pass
